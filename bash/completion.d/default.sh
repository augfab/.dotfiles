#!/bin/bash
# vim:ts=4:sw=4:et:tw=79:ft=sh:

# Try to load existing bash completion scripts

if [ -r /usr/share/bash-completion/bash_completion ]
then
    . /usr/share/bash-completion/bash_completion
elif [ -r /etc/bash_completion ]
then
    . /etc/bash_completion
elif [ -d /etc/bash_completion.d/ ]
then
    . /etc/bash_completion.d/*
fi
