#!/bin/bash
# vim:ts=4:sw=4:et:tw=79:ft=sh:

if [ "x${0%%/*}" != "x." ]
then
    >&2 printf "%s: must be called from root of directory.\n" ${0##*/}
    exit 1
fi

repository="$PWD"

local_dotfiles="$HOME/.local/dotfiles"
mkdir -p "$local_dotfiles"

link_dotfile () {
    if [ "$#" -ne 2 ]
    then
        >&2 printf "%s:%d\n" ${0##*/} $LINENO
        >&2 printf "Function %s needs two arguments:\n" $FUNCNAME
        >&2 printf "%s target linkname\n" $FUNCNAME
        exit 1
    elif ! [ -h "$2" ] && [ -f "$2" -o -d "$2" ]
    then
        printf "Moving existing %s to %s\n" "$2" "$local_dotfiles/$1"
        mv -i "$2" "$local_dotfiles/$1"
    fi
    ln -si "$repository/$1" "$2"
}

file="$local_dotfiles/repository"
if [ -f "$file" ] && ! [ -w "$file" ]
then
    chmod u+w "$file"
fi

printf "%s\n" "$repository" > "$file"
chmod -w "$file"

link_dotfile "bash/bashrc"          "$HOME/.bashrc"
link_dotfile "bash/bash_profile"    "$HOME/.bash_profile"
link_dotfile "bash/bash_logout"     "$HOME/.bash_logout"
link_dotfile "vim/vimrc"            "$HOME/.vimrc"
link_dotfile "git/gitconfig"        "$HOME/.gitconfig"
link_dotfile "vim/vim"              "$HOME/.vim"


case "$HOSTNAME" in
doriath)
    link_dotfile "rtorrent/rtorrent.rc" "$HOME/.rtorrent.rc"
    link_dotfile "X11/xinitrc"          "$HOME/.xinitrc"
    link_dotfile "X11/Xresources"       "$HOME/.Xresources"
    ;;
erebor)
    link_dotfile "rtorrent/rtorrent.rc" "$HOME/.rtorrent.rc"
    ;;
*.nice.arm.com)
    link_dotfile "X11/xinitrc"          "$HOME/.xinitrc"
    link_dotfile "X11/Xresources"       "$HOME/.Xresources"
    ;;
*)
    ;;
esac
